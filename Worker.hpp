#ifndef WORKER_HPP
#define WORKER_HPP

#include "WorkerActor.hpp"
#include <simgrid/s4u.hpp>

class Worker {

private:
    WorkerActor worker;
    simgrid::s4u::Host* host;

public:
    Worker(simgrid::s4u::Host* host);
    WorkerActor getWorkerActor();
    simgrid::s4u::Host* getHost();
    void shutDown();
    void bootUP();
    int getNAvailableCores();
    
};

#endif