#ifndef MANAGER_HPP
#define MANAGER_HPP

#include <simgrid/s4u.hpp>
#include <queue> 
#include "VMRequest.hpp"
#include "Message.hpp"
#include "WorkerActor.hpp"
#include "ElectricDC.hpp"
#include "PVActor.hpp"


class Manager {
private:
  long tasks_count              = 0;
  int compute_cost              = 0;
  double communicate_cost       = 0;  
  std::vector<simgrid::s4u::Mailbox*> dcs;
  simgrid::s4u::CommPtr comm_received = nullptr; 
  simgrid::s4u::Mailbox* mailbox_;
  double maxSimDuration = 3000;
  std::queue <VMRequest*> vmRequestQueue;  
  void createWorkers();

public:
  explicit Manager(std::vector<std::string> args);
  void operator()();

};
#endif