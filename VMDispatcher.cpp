
#include "VMDispatcher.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(vmDispatcher, "vmDispatcher category");

VMDispatcher::VMDispatcher(std::vector<std::string> args)
{
    XBT_INFO("criou o vmdispatcherrr");
    const char* actor_name     = args.at(0).c_str();
    const char* trace_filename = args.size() > 1 ? args[1].c_str() : nullptr;
    XBT_INFO("ARGS SIZE %d", args.size());
    XBT_INFO("actor_name");
    XBT_INFO(actor_name);
    XBT_INFO("trace_filename");    
    XBT_INFO(trace_filename);
    simgrid::xbt::replay_runner(actor_name, trace_filename);
}

void VMDispatcher::operator()()
{
// Nothing to do here
}


void updateEnergy(){
    simgrid::s4u::Mailbox *  mailboxDC = simgrid::s4u::Mailbox::by_name("grid");    
    mailboxDC->put_init(new Message(MESSAGE_UPDATE_ENERGY),1)->detach(); 
}

VMRequest* buildVMRequest(simgrid::xbt::ReplayAction& action){    
    XBT_INFO("CRIANDO O VM REQUEST");    
    int ram   =   std::stoi(action[3]);    
    int cores =   std::stoi(action[4]);    
    std::string id = std::string(action[5]);    
    return new VMRequest(cores,ram,std::string(id));
}

void sendVMMessage(VMRequest* vm){
    simgrid::s4u::Mailbox* mailbox_;
    mailbox_=simgrid::s4u::Mailbox::by_name("manager");
    double data = 999;
    Message* message = new Message(MESSAGE_VM_SUBMISSION,vm);
    mailbox_ ->put_init(message,1)->detach();
}

void VMDispatcher::sendVM(simgrid::xbt::ReplayAction& action){
    XBT_INFO("RECEBI REQUEST PRA MANDAR");    
    double time  =  std::stod(action[2]);
    XBT_INFO("JAJA EU VOu MANDAR");                
    waitFor(time);
    buildVMRequest(action);
    VMRequest* vmtobesent = buildVMRequest(action);
    XBT_INFO("VM BUILT INFO, RAM %d CORES %d ID %s",vmtobesent->getRamSize(),vmtobesent->getNCores(),vmtobesent->getID());
    sendVMMessage(vmtobesent);
    XBT_INFO("EU MANDEI");
    XBT_INFO("VO ATUALIZAR ENERGIA");
    updateEnergy();
}

void VMDispatcher::stop(simgrid::xbt::ReplayAction& action){
    double time  =  std::stod(action[2]);
    waitFor(time);
    simgrid::s4u::Actor::kill_all();
    simgrid::s4u::this_actor::exit();
}

void VMDispatcher::waitFor(double time){
    double waitForTime = time - simgrid::s4u::Engine::get_clock();
    XBT_INFO("CURRENT TIME %f",simgrid::s4u::Engine::get_clock());
    if(waitForTime>0){
        XBT_INFO("I WILL WAIT FOR %f",waitForTime);
        simgrid::s4u::this_actor::sleep_for(waitForTime);
        XBT_INFO("CURRENT TIME %f",simgrid::s4u::Engine::get_clock());
    }     
}

