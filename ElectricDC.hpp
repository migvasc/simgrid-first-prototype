#ifndef ELECTRICDC_HPP
#define ELECTRICDC_HPP

#include "ElectricSystem.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>  
#include <stdlib.h>
#include <simgrid/s4u.hpp>
#include "simgrid/plugins/energy.h"
#include "Message.hpp"
#include "EnergyInformation.hpp"

using namespace std;

/**
 * 
 * The electric system of a single data-center.
 *
 */
class ElectricDC : ElectricSystem
{

private:	
	/**
	 * The name of the data-center.	
	 */
	string id;	
	/**
	 * The number of photo-voltaic panel in the data-center.
	 * Used to scale the photo-voltaic power production.
	 */
	int nbPV;
	/**
	 * The power output stream of the system.
	 */
	ofstream* power_output;	    
	simgrid::s4u::CommPtr comm_received = nullptr;   	
	simgrid::s4u::Mailbox* mailboxPV;    
	simgrid::s4u::Mailbox* mailboxDC;	
		

	/**
	 * Compute the current cumulative energy consumption of the data-centers according to SimGrid.
	 * @return the current cumulative energy consumption of the data-centers.
	 */
	double computeDcCurrentEnergyConsumption();				
	/**
	 * Return the current local power production of the DC.
	 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
	 * @return The current local power production of the DC.
	 */
	double getLocalPowerProduction();
	/**
	 * Return the power available for the local consumption AND for the output migrations.
	 * It is equals to: local_production + input_migration.
	 * @return The power available for the local consumption AND for the output migrations.
	 */
	double getAvailablePower();
	
	void sendWorkerMailBox();

public:

	/**
	 * Creates the electric system of a single data-center.
	 */
	explicit ElectricDC(std::string aID, int nPV);    
	/**
	 * Return the power currently available for the local consumption.
	 * It is equals to: local_production + input_migration - output_migration
	 * @return The current production of the data-center.
	 */	
    double getCurrentPowerProduction();
    /**
	 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
	 * (called by the master at the end of the simulation).
	 */		
    void operator()();		
	string getName();
    virtual void updateEnergy(double newTime);
    virtual void stop();
	vector<simgrid::s4u::Host*> getHosts();
	std::vector<double> getEnergyInformation();
protected:
	/**
	 * Log the current power trajectories of the system in the second output file (format = time;consumption;production+In-Out;production-Out;production+In;-Out).
	 */
	 void logPowerResult(double consumption, 
						double productionInOut, 
						double productionOut, 
						double productionIn, 
						double out);
};


#endif