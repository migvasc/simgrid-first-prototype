#ifndef WORKERACTOR_HPP
#define WORKERACTOR_HPP


#include <simgrid/s4u.hpp>
#include "VMRequest.hpp"

class WorkerActor {

  simgrid::s4u::Mailbox* mailbox = nullptr;
  simgrid::s4u::CommPtr comm_received = nullptr; // current comm
  double maxSimDuration = 3000;

public:
  explicit WorkerActor(std::string name);  
  void operator()();

};

#endif