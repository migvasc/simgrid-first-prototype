#include "ElectricDC.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(edc, "edc category");

/**
 * Creates the electric system of a single data-center.
 */
ElectricDC::ElectricDC(std::string aID, int nPV) : ElectricSystem(("output/energy_"+aID+".csv")) 
{             
    id = aID;							        
    nbPV = nPV;
    std::string id_mailboxpv = "pv_" +id  ;
    std::string id_mailboxdc = "dc_" +id  ;
    mailboxPV = simgrid::s4u::Mailbox::by_name(id_mailboxpv );
    mailboxDC = simgrid::s4u::Mailbox::by_name(id_mailboxdc);           
    try {

        power_output = new ofstream();
        power_output->open(("output/power_"+id+".csv"));
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what()   << endl;
    }			
}


vector<simgrid::s4u::Host*> ElectricDC::getHosts(){
    vector<simgrid::s4u::Host*> hosts = simgrid::s4u::Engine::get_instance()
                                                    -> netzone_by_name_or_null(id)->get_all_hosts();        
    return hosts;
}

void ElectricDC::sendWorkerMailBox(){
    vector<simgrid::s4u::Host*> hostList = getHosts();
    simgrid::s4u::Host* aHost = hostList.front();
    simgrid::s4u::Mailbox* result = simgrid::s4u::Mailbox::by_name(aHost->get_name());
    simgrid::s4u::Mailbox* masterMailBox = simgrid::s4u::Mailbox::by_name("manager");
    Message* m = new Message(MESSAGE_HOST_RESPONSE,result);    
    masterMailBox->put_init(m,1)->detach();
}

void ElectricDC::operator()(){
    void* data = nullptr;
    while(true){
        if (comm_received == nullptr) 
        {
            comm_received = mailboxDC->get_async(&data);
        }
        if (comm_received->test())
        {            
            Message* message = static_cast<Message*>(data);        

            switch (message->type)
            {
                case MESSAGE_UPDATE_ENERGY:
                    updateEnergy(simgrid::s4u::Engine::get_clock());
                    break;    
                case MESSAGE_GET_WORKERS:      
                    sendWorkerMailBox();
                    break;                
                case MESSAGE_GET_ENERGY:
                    getEnergyInformation();
                    break;
                default:
                    break;
            }
            comm_received = nullptr;
        } 
        else {
            simgrid::s4u::this_actor::sleep_for(0.01);
        }
    }
}


double getDCEnergyConsumption(std::string dcName){
    double power = 0;
    double energy =0;
    std::vector<simgrid::s4u::Host*> hosts = simgrid::s4u::Engine::get_instance()
                                                    ->netzone_by_name_or_null(dcName)->get_all_hosts();        
    if(not hosts.empty())
        for(simgrid::s4u::Host* h : hosts ){
            power = sg_host_get_consumed_energy(h);
            energy += power;    
        }

    return energy;
}


/**
 * Compute the current cumulative energy consumption of the data-centers according to SimGrid.
 * @return the current cumulative energy consumption of the data-centers.
 */
double ElectricDC::computeDcCurrentEnergyConsumption() {        
    double energy = 0;    
    double power = 0;
    std::vector<simgrid::s4u::Host*> hosts = getHosts();        
    sg_host_energy_update_all();    
    if(not hosts.empty())
    {
        for(simgrid::s4u::Host* h : hosts )        
        {
            power = sg_host_get_consumed_energy(h);
            energy += power;
        }
    }
    return energy;
}


/**
 * Return the current local power production of the DC.
 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
 * @return The current local power production of the DC.
 */
double ElectricDC::getLocalPowerProduction(){
    void * data = mailboxPV->get(10000);    
    double *_currentGreenPowerProduction =  static_cast<double*>(data);    
    XBT_INFO("%f",*_currentGreenPowerProduction);        
    double result = *_currentGreenPowerProduction;
    return  result * nbPV;
}

/**
 * Return the power available for the local consumption AND for the output migrations.
 * It is equals to: local_production + input_migration.
 * @return The power available for the local consumption AND for the output migrations.
 */
double ElectricDC::getAvailablePower(){									
    return getLocalPowerProduction() ;
}


void ElectricDC::updateEnergy(double newTime) {	
    double greenPowerProduction = getLocalPowerProduction();
    double newCumulativeEnergyConsumption = computeDcCurrentEnergyConsumption();			
    double timeStepGreenEnergyProduction = (max(0.0,greenPowerProduction)) * (newTime - time);
    double timeStepEnergyConsumption = newCumulativeEnergyConsumption - cumulativeEnergyConsumption;
    double brownTimeStepEnergyConsumption = max(0.0, timeStepEnergyConsumption - timeStepGreenEnergyProduction);
    double timeStepRemainingGreenEnergy = max(0.0, timeStepGreenEnergyProduction - timeStepEnergyConsumption);		
    cumulativeBrownEnergyConsumption += brownTimeStepEnergyConsumption;	
    cumulativeEnergyConsumption = newCumulativeEnergyConsumption;
    cumulativeGreenEnergyProduction += timeStepGreenEnergyProduction;
    cumulativeRemainingGreenEnergy += timeStepRemainingGreenEnergy;				
    double timeStepPowerConsumption = timeStepEnergyConsumption / (newTime - time);		
    time = newTime;			
    logResult();		
    double powInOut = greenPowerProduction;                    
    double powIn = greenPowerProduction;
    double out = powInOut - powIn;
    double powOut = greenPowerProduction + out;		
    logPowerResult(timeStepPowerConsumption, powInOut, powOut, powIn, out);
}



/**
 * Return the power currently available for the local consumption.
 * It is equals to: local_production + input_migration - output_migration
 * @return The current production of the data-center.
 */

double ElectricDC::getCurrentPowerProduction(){		
    // first we compute the power losses caused by the output migrations        
    // then we substract the losses to the available power
    return max(0.0, getAvailablePower());
}

    /**
 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
 * (called by the master at the end of the simulation).
 */
void ElectricDC::stop(){
    try {
        power_output->close();
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what()   << endl;
    }    	
}

string ElectricDC::getName(){
    return simgrid::s4u::this_actor::get_cname();
}

/**
 * Log the current power trajectories of the system in the second output file (format = time;consumption;production+In-Out;production-Out;production+In;-Out).
 */
void ElectricDC::logPowerResult(double consumption, double productionInOut, double productionOut, double productionIn, double out) {
    string separator = ";";//MasterVMManager.FILE_SEPARATOR;
    try {
        string log = std::to_string(time) 
                    + separator 
                    + std::to_string(consumption)
                    + separator
                    + std::to_string(productionInOut)
                    + separator
                    + std::to_string(productionOut)
                    + separator
                    + std::to_string(productionIn)
                    + separator
                    + std::to_string(out);
        XBT_INFO(log.c_str());
        *power_output << log << endl;
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what()   << endl;
    }	
}


std::vector<double> ElectricDC::getEnergyInformation()
{
     simgrid::s4u::Mailbox* gridMailBox = simgrid::s4u::Mailbox::by_name("grid_get_energy_info");    
     gridMailBox->put_init(new EnergyInformation(cumulativeBrownEnergyConsumption,
                                                    cumulativeEnergyConsumption,
                                                    cumulativeGreenEnergyProduction,
                                                    cumulativeRemainingGreenEnergy),
                            0)->detach();      
}