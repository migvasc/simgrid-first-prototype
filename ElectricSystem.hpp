#ifndef ELECTRICSYSTEM_HPP
#define ELECTRICSYSTEM_HPP


/**
 * An electric system of a distributed cloud.
 * According to the cloud consumption given by SimGrid, and to the green power production given by the traces (monitored by some dedicated process), it computes the cumulative :
 * 		- energy consumption,
 * 		- brown energy consumption,
 * 		- green energy production.
 * The system produce an output files which log these energy trajectories.
 * The system must be manually updated (through the master) : 
 * 		- by the workers when they change their power consumption,
 * 		- and by the photo-voltaic monitoring processes when the power production has changed.
 *
 */

#include <string>
#include <iostream>
#include <fstream>

using namespace std;


class ElectricSystem
{
protected:
    /**
	 * The cumulative energy consumption of the system.
	 */
	double cumulativeEnergyConsumption;
	
	/**
	 * The cumulative brown energy consumption of the system.
	 */
	double cumulativeBrownEnergyConsumption;
	
	/**
	 * The cumulative green energy production of the system.
	 */
	double cumulativeGreenEnergyProduction;
	
	double cumulativeRemainingGreenEnergy;

	/**
	 * The time of the last update.
	 */
	double time;

    /**
	 * Log the current state of the system in the output file (format = time;consumption;green-production;brown-consumption).
	 */    
	void logResult();

public:
    double getCumulativeRemainingGreenEnergy();

    ElectricSystem();
    /**
	 * Initialize the system.
	 * @param outputFile The output file of the system.
	 */
	ElectricSystem(std::string outputFile);

	/**
	 * Return the current cumulative energy consumption of system.
	 * @return The current cumulative energy consumption of system.
	 */
	double getCumulativeEnergyConsumption();
	
	/**
	 * Return the current cumulative brown energy consumption of system.
	 * @return The current cumulative brown energy consumption of system.
	 */
	double getCumulativeBrownEnergyConsumption();

	/**
	 * Return the current cumulative green energy production of system.
	 * @return The current cumulative green energy production of system.
	 */
	double getCumulativeGreenEnergyProduction();

	/**
	 * Update the cumulative energies.
	 * @param newTime the new time for the update.
	 */
	virtual void updateEnergy(double newTime) = 0;
	
	
	/**
	 * Stop the system -i.e. close the output stream (called by the master at the end of the simulation).
	 */
	void stop();

private:
	/**
	 * The output stream of the system.
	 */
	ofstream *output;    
};



#endif