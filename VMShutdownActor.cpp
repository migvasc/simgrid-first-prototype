#include "VMShutdownActor.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(vmshutdown, "vmshutdown category");

VMShutdownActor::VMShutdownActor(DeployableVM* avm){
    vm=avm;
}
void VMShutdownActor::operator()(){
    XBT_INFO("THIS VM ( %s ) WILL BE KILLED AFTER %f S",vm->getVM()->get_cname(),vm->getDuration());
    simgrid::s4u::this_actor::sleep_for(vm->getDuration());
    XBT_INFO("KILLING VM %s ",vm->getVM()->get_cname());
    vm->getVM()->destroy();
    simgrid::s4u::this_actor::exit();
}
