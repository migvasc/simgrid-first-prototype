#include "WorkerActor.hpp"
#include "DeployableVM.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(workerActor, "workeractor category");


WorkerActor::WorkerActor(std::string name)
{
    mailbox = simgrid::s4u::Mailbox::by_name(name);
}

void WorkerActor::operator()()
{

    double compute_cost;
    double* msg;
    void* data;
    
    while(true)
    {
        if(comm_received ==nullptr)
        {
            comm_received = mailbox->get_async(&data);
        }
        if(comm_received->test())
        {
            XBT_INFO("Worker received msg");
            VMRequest* vm = static_cast<VMRequest*>(data);
            XBT_INFO("This VM will be started %s",vm->getID().c_str());
            DeployableVM* dvm = new DeployableVM(vm->getID(),simgrid::s4u::this_actor::get_host(),vm->getNCores(),vm->getNCores()*10.0);
            dvm->startVM();
            comm_received =nullptr;
            std::string id = std::to_string(simgrid::s4u::Engine::get_clock());
        }
        else
        {
            simgrid::s4u::this_actor::sleep_for(0.01);
        }

    }

    comm_received = nullptr;
    XBT_INFO("Exiting now.");

}
