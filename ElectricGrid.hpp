#ifndef ELECTRICGRID_HPP
#define ELECTRICGRID_HPP

#include <simgrid/s4u.hpp>
#include "ElectricSystem.hpp"
#include "Message.hpp"
#include "EnergyInformation.hpp"

/**
 * The electric system of the cloud.
 * This system is composed of several subsystems : the electric systems of the data-centers composing the cloud.
 * 
 */
class ElectricGrid : ElectricSystem {

private:
	simgrid::s4u::Mailbox* mailboxGrid;    
    simgrid::s4u::CommPtr comm_received = nullptr;   	
    std::vector<simgrid::s4u::Mailbox*> dcs;
    virtual void updateEnergy(double newTime);


public:

	/**
	 * Creates the electric system of a single data-center.
	 */
	explicit ElectricGrid(std::vector<std::string> args);

    /**
	 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
	 * (called by the master at the end of the simulation).
	 */		
    void operator()();

};

#endif