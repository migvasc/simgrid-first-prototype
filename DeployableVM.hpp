#ifndef DEPLOYABLEVM_HPP
#define DEPLOYABLEVM_HPP
#include "simgrid/s4u.hpp"
#include "VMShutdownActor.hpp"

class VMShutdownActor;
class DeployableVM{

private: 
    simgrid::s4u::VirtualMachine* vm;
    double startTime;
    double duration;
public:
    DeployableVM(std::string id, simgrid::s4u::Host* host, int ncores, double duration);
    simgrid::s4u::VirtualMachine* getVM();
    double getRuntime();
    double getDuration();
    void startVM();
    void startsExecuting();
    static void computation_fun();
};

#endif