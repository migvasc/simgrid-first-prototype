#include "PVActor.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(pv, "pv category");

 PVActor::PVActor() = default;
 PVActor::PVActor(std::string input, std::string dcname){		
    XBT_INFO("PV ACTOR created");
    
    inputFile = input;
    dcName = dcname;
    time = 0;    
    std::string id_mailboxpv= "pv_" +dcName ;
    mailbox_ = simgrid::s4u::Mailbox::by_name(id_mailboxpv);    
    
    XBT_INFO("PV -> IDS MAIL BOXES, pv %s",id_mailboxpv.c_str());
    currentGreenPowerProduction = 0;
} 

void PVActor::operator()() {     
    string logInfo = "I start monitoring the green power production of DC " +getName();
    XBT_INFO(logInfo.c_str());
    ifstream input;
    try {

        input =  ifstream(inputFile);
        advanceTime(&input);  
        
        time = 0;
        currentGreenPowerProduction = nextGreenPowerProduction;

        while(nextTime != numeric_limits<double>::max() ){               
            double sleep_amount = nextTime - time;                        
           // XBT_INFO(to_string(sleep_amount).c_str());
            sendMessage(sleep_amount);
            advanceTime(&input);
            logInfo = "new green power production for DC "+getName()+" : "+ to_string(currentGreenPowerProduction)+"W";
            //XBT_INFO(logInfo.c_str());            
        }    
        input.close();            

    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;

    }	
    
}


/**
 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
 */
void PVActor::advanceTime(ifstream *input) {
     simgrid::s4u::CommPtr ptr = nullptr;
    try {
        string  nextLine;
        
        getline(*input,nextLine);
        if(!nextLine.empty())
        {            
            //XBT_INFO("OLD currentGreenPowerProduction");
            //XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            currentGreenPowerProduction = nextGreenPowerProduction;
            //XBT_INFO("UPDATE currentGreenPowerProduction");
            //XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            std::vector<std::string> nextInput;				
            // TODO: GET FROM THIS FILE THE SEPARATOR
            // std::string nextInput ;//= nextLine.split(MasterVMManager.FILE_SEPARATOR);
            boost::algorithm::split(nextInput, nextLine, boost::is_any_of(";"));
            nextTime = atof(nextInput[0].c_str());
            nextGreenPowerProduction = atof(nextInput[1].c_str());
            time = simgrid::s4u::Engine::get_clock();				            
        }
        else 
        {
            nextTime = numeric_limits<double>::max();
        }

    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;

    }		
      
}

void PVActor::sendMessage(double time){    
    while(time > 0){
        //If exists a "get" message in the queue
        if(not mailbox_->empty()){        
            mailbox_->put(new double(currentGreenPowerProduction),0);
        }
        time --;
        simgrid::s4u::this_actor::sleep_for(.0001);			
    }
}


string PVActor::getName(){
    return simgrid::s4u::this_actor::get_cname();
}

