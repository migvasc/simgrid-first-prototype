#ifndef MESSAGE_HPP
#define MESSAGE_HPP


/** Types of messages exchanged between two peers. */
enum e_message_type {
  MESSAGE_VM_SUBMISSION,
  MESSAGE_UPDATE_ENERGY,
  MESSAGE_GET_WORKERS,
  MESSAGE_GET_ENERGY,
  MESSAGE_HOST_RESPONSE
};

class Message{

public: 

    e_message_type type;
    void* data;
    
    Message(e_message_type type_, void* someData){
        type = type_;
        data = someData;
    }

    Message(e_message_type type_){
        type = type_;
        data = nullptr;
    }

};

#endif