#include "DeployableVM.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(dvm, "DeployableVM category");

DeployableVM::DeployableVM(std::string id, simgrid::s4u::Host* host, int ncores, double time){    
    duration = time;
    vm = new simgrid::s4u::VirtualMachine(id,host,ncores);
}

simgrid::s4u::VirtualMachine* DeployableVM::getVM(){
    return vm;
}

double DeployableVM::getRuntime(){
    double now = simgrid::s4u::Engine::get_clock() ;
    return now - startTime;
}

double DeployableVM::getDuration(){
    return duration;
}

void DeployableVM::startVM(){
    startTime = simgrid::s4u::Engine::get_clock();    
    startTime = simgrid::s4u::Engine::get_clock();    
    vm->start();    
    startsExecuting();


}



void DeployableVM::startsExecuting()
{
    for(int i=0; i < vm->get_core_count(); i++){
        simgrid::s4u::ActorPtr ptr =  simgrid::s4u::Actor::create(std::string("id")+ std::to_string(i) +vm->get_cname(), vm, computation_fun);     
    }                            
    //Creates the process that will kill the VM after it finishes executing
    simgrid::s4u::Actor::create("killer_"+vm->get_name(), vm->get_pm(), VMShutdownActor(this));    
}

void DeployableVM::computation_fun(){
    double clock_sta = simgrid::s4u::Engine::get_clock();
    simgrid::s4u::this_actor::execute(8024000000.0 * 10000000); // A great number of flops because the VM will use fully the core for the amount of time of its duration
    double clock_end = simgrid::s4u::Engine::get_clock();
    XBT_INFO("%s:%s executed %g", simgrid::s4u::this_actor::get_host()->get_cname(),
    simgrid::s4u::this_actor::get_cname(), clock_end - clock_sta);
}
