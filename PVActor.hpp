#ifndef PVACTOR_HPP
#define PVACTOR_HPP


#include <iostream>
#include <fstream>
#include <simgrid/s4u.hpp>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include <limits>

using namespace std;

class PVActor 
{
	/**
	 * The input file containing the power production trajectory.
	 */
    string inputFile;
	

	string dcName; 
	
	/**
	 * The next green power production according to the input trace.
	 */
	double nextGreenPowerProduction;
	
	/**
	 * The current green power production according to the input trace.
	 */
	double currentGreenPowerProduction;
	
	/**
	 * the last time the power production was updated.
	 */
	double time;
	
	/**
	 * the next time the power production will be updated.
	 */
	double nextTime;

    /**
	 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
	 */
	void advanceTime(ifstream *input);

	string getName();

	simgrid::s4u::Mailbox* mailbox_;

	void sendMessage(double time);

public:
    
	explicit PVActor();
    explicit PVActor(std::string input, std::string dcname);
    void operator()();
    


};

#endif