#include "ElectricGrid.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(egrid, "electric grid category");

ElectricGrid::ElectricGrid(std::vector<std::string> args) : ElectricSystem("output/global_energy.csv") 
{   
    mailboxGrid = simgrid::s4u::Mailbox::by_name("grid");          
    for (unsigned int i = 1; i < args.size(); i++){
      dcs.push_back(simgrid::s4u::Mailbox::by_name("dc_"+args[i]));
    }
}

void ElectricGrid::operator()()
{
    void* data = nullptr;
    while(true){
        if (comm_received == nullptr) 
        {
            comm_received = mailboxGrid->get_async(&data);
        }
        if (comm_received->test())
        {            
            Message* message = static_cast<Message*>(data);        
            switch (message->type)
            {
                case MESSAGE_UPDATE_ENERGY:            
                    updateEnergy(simgrid::s4u::Engine::get_clock());
                    break;                
                default:
                    break;
            }

            comm_received = nullptr;
        } 
        else {
            simgrid::s4u::this_actor::sleep_for(0.01);
        }
    }    
}


void ElectricGrid::updateEnergy(double newTime)
{
    for(simgrid::s4u::Mailbox* mailbox : dcs)
    {
        mailbox->put_init(new Message(MESSAGE_UPDATE_ENERGY),0)->detach();
    }

    simgrid::s4u::Mailbox* energyInfoMB = simgrid::s4u::Mailbox::by_name("grid_get_energy_info");
    energyInfoMB->set_receiver(simgrid::s4u::Actor::self());

    int messagesToReceive = dcs.size();   
    cumulativeBrownEnergyConsumption = 0;
    cumulativeEnergyConsumption = 0;
    cumulativeGreenEnergyProduction = 0;
    cumulativeRemainingGreenEnergy = 0;
        
  
    for(simgrid::s4u::Mailbox* mailbox : dcs)
    {
        mailbox->put_init(new Message(MESSAGE_GET_ENERGY),0)->detach();
    }

    while(messagesToReceive > 0)
    {
        if(energyInfoMB->ready())
        {
    
            EnergyInformation* info = static_cast<EnergyInformation*>(energyInfoMB->get());
            messagesToReceive--;           
            cumulativeBrownEnergyConsumption += info->getCumulativeBrownEnergyConsumption();
            cumulativeEnergyConsumption += info->getCumulativeEnergyConsumption();
            cumulativeGreenEnergyProduction += info->getCumulativeGreenEnergyProduction();
            cumulativeRemainingGreenEnergy += info->getCumulativeRemainingGreenEnergy();
        }
        else
        {
           simgrid::s4u::this_actor::sleep_for(.01);      
        }
    }

    time = simgrid::s4u::Engine::get_clock();
  	logResult();      

}
