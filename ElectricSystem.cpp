
/**
 * An electric system of a distributed cloud.
 * According to the cloud consumption given by SimGrid, and to the green power production given by the traces (monitored by some dedicated process), it computes the cumulative :
 * 		- energy consumption,
 * 		- brown energy consumption,
 * 		- green energy production.
 * The system produce an output files which log these energy trajectories.
 * The system must be manually updated (through the master) : 
 * 		- by the workers when they change their power consumption,
 * 		- and by the photo-voltaic monitoring processes when the power production has changed.
 *
 */

#include <string>
#include <iostream>
#include <fstream>
#include "ElectricSystem.hpp"
#include <simgrid/s4u.hpp>

XBT_LOG_NEW_DEFAULT_CATEGORY(es, "edc category");
/**
 * Log the current state of the system in the output file (format = time;consumption;green-production;brown-consumption).
 */    
void ElectricSystem::logResult() {                                    
    // TODO:  : MasterVMManager.FILE_SEPARATOR;
    std::string separator = ";";        		
    try         
    {            
            std::string logString = std::to_string(time)            
            + separator
            + std::to_string(cumulativeEnergyConsumption)
            + separator
            + std::to_string(cumulativeGreenEnergyProduction)
            + separator
            + std::to_string( cumulativeBrownEnergyConsumption);

        *output <<  logString << endl;

    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;
    }

}

double ElectricSystem::getCumulativeRemainingGreenEnergy()
{
    return cumulativeRemainingGreenEnergy;
}


ElectricSystem::ElectricSystem() = default;
/**
 * Initialize the system.
 * @param outputFile The output file of the system.
 */
ElectricSystem::ElectricSystem(std::string outputFile){
    time = -1;
    try 
    { 
        output = new ofstream();
        output->open(outputFile);            
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;
    }

    cumulativeEnergyConsumption = 0;
    cumulativeGreenEnergyProduction = 0;
    cumulativeBrownEnergyConsumption = 0;
    cumulativeRemainingGreenEnergy = 0;
}

/**
 * Return the current cumulative energy consumption of system.
 * @return The current cumulative energy consumption of system.
 */
double ElectricSystem::getCumulativeEnergyConsumption() {
    return cumulativeEnergyConsumption;
}

/**
 * Return the current cumulative brown energy consumption of system.
 * @return The current cumulative brown energy consumption of system.
 */
double ElectricSystem::getCumulativeBrownEnergyConsumption() {
    return cumulativeBrownEnergyConsumption;
}

/**
 * Return the current cumulative green energy production of system.
 * @return The current cumulative green energy production of system.
 */
double ElectricSystem::getCumulativeGreenEnergyProduction() {
    return cumulativeGreenEnergyProduction;
}


/**
 * Stop the system -i.e. close the output stream (called by the master at the end of the simulation).
 */
void ElectricSystem::stop(){
    try {
        output->close();
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;
    }
}