#include "VMRequest.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(vmrequest, "vmrequest category");

VMRequest::VMRequest(int cores, int ram, std::string vmID){
    XBT_INFO("CONSTRUTOR DA VM: cores %d, ram %d, ID %s ",cores,ram,vmID.c_str());
    ncores = cores;
    ramsize = ram;    
    id = vmID;
}

int VMRequest::getRamSize(){
    return ramsize;
}
int VMRequest::getNCores(){
    return ncores;
}

std::string VMRequest::getID(){
    return id;
}

simgrid::s4u::VirtualMachine* VMRequest::getVM(){
    return vm;
}

void VMRequest::setVM(simgrid::s4u::VirtualMachine* vm_){
    vm = vm_;
}

