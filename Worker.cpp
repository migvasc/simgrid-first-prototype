
void Manager::bootup(simgrid::s4u::Host* host)
{
  int previous_pstate = host->get_pstate();
  XBT_INFO("Switch to virtual pstate 3, that encodes the 'booting up' state in that platform");
  host->set_pstate(0);
  XBT_INFO("Actually start the host");
  host->turn_on();
  XBT_INFO("Wait 150s to simulate the boot time.");
  simgrid::s4u::this_actor::sleep_for(150);
  XBT_INFO("The host is now up and running. Switch back to previous pstate %d", previous_pstate);
  host->set_pstate(0);
}

void Manager::shutdown(simgrid::s4u::Host* host)
{
  int previous_pstate = host->get_pstate();
  XBT_INFO("Switch to virtual pstate 4, that encodes the 'shutting down' state in that platform");
  host->set_pstate(1);
  XBT_INFO("Wait 10 seconds to simulate the shutdown time.");
  simgrid::s4u::this_actor::sleep_for(10);
  XBT_INFO("Switch back to previous pstate %d, that will be used on reboot.", previous_pstate);
  //host->set_pstate(previous_pstate);
  XBT_INFO("Actually shutdown the host");
  host->turn_off();
}