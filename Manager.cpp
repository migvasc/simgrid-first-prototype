#include "Manager.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(manager, "manager category");


/**
 * Manages the distributed cloud with on-site green (e.g. solar) energy productions.
 * The cloud is composed of heterogeneous hosts. 
 * The manager is in charge of dispatching an incoming workload to the different data-centers.
 * Rather than directly interacting with the hosts, the manager deploy and interact with worker applications which encapsulate the hosts desired behavior. 
 * The workload is composed of VMs which require a given number of cores and has to be executed for a given period of time.
 * The master, and therefore the workload, have a time-stepped behavior.
 *
 */
Manager::Manager(std::vector<std::string> args)
{

    simgrid::s4u::Host* host = simgrid::s4u::this_actor::get_host();
    /* Creates the mailboxes and the ElectricDC and PVActor actors */
    for (unsigned int i = 1; i <=5; i++){      
      dcs.push_back(simgrid::s4u::Mailbox::by_name("dc_"+args[i]));
      simgrid::s4u::Actor::create("dc_"+args[i], host, ElectricDC(args[i],1));     
      simgrid::s4u::Actor::create("pv_"+args[i], host, PVActor(args[i+10],args[i]));    
    }
    mailbox_=simgrid::s4u::Mailbox::by_name("manager");
    createWorkers();
}


/* "main" method */
void Manager::operator()()
{  
    void* data = nullptr;
    int selected = 0;    
    VMRequest* vm = nullptr;    
    simgrid::s4u::Mailbox* mailbox = nullptr;
    simgrid::s4u::Mailbox* hostMailbox = nullptr;

    void* msgData;
    while(true){
        if (comm_received == nullptr) 
        {
            comm_received = mailbox_->get_async(&data);
        }
        if (comm_received->test())
        {          
            Message* message = static_cast<Message*>(data);                  
            switch (message->type)
            {
                case MESSAGE_VM_SUBMISSION:
                    vm = static_cast<VMRequest*>(message->data);        
                    compute_cost  = vm->getRamSize() ;        
                    vmRequestQueue.push(vm);
                    mailbox = dcs[selected % dcs.size() ];
                    mailbox->put_init(new Message(MESSAGE_GET_WORKERS), communicate_cost)->detach();
                    selected++;                        
                    break;                                      
                case MESSAGE_HOST_RESPONSE:              
                    vm = vmRequestQueue.front();
                    vmRequestQueue.pop();
                    hostMailbox  = static_cast<simgrid::s4u::Mailbox*>(message->data); 
                    hostMailbox->put_init(vm, communicate_cost)->detach();
                default:
                  break;
              }          
            comm_received = nullptr;
        }
        else {
            simgrid::s4u::this_actor::sleep_for(0.01);
        }        
    }      
    comm_received = nullptr;    
}


void Manager::createWorkers()
{           
  std::vector<simgrid::s4u::Host*> allHosts = simgrid::s4u::Engine::get_instance()->get_all_hosts();
  for(simgrid::s4u::Host* host : allHosts)
  {    
      if(host!= simgrid::s4u::this_actor::get_host())
      {
        simgrid::s4u::ActorPtr someActor = simgrid::s4u::Actor::create("worker_"+host->get_name(), host, WorkerActor(host->get_name()));     
        someActor->set_auto_restart(true);
       }
  }
}

