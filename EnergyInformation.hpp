#ifndef ENERGYINFORMATION_HPP
#define ENERGYINFORMATION_HPP

class EnergyInformation{

private:
    double cumulativeBrownEnergyConsumption = 0;
    double cumulativeEnergyConsumption = 0;
    double cumulativeGreenEnergyProduction = 0;
    double cumulativeRemainingGreenEnergy = 0;

public:
    EnergyInformation() = default;
    EnergyInformation(double _cumulativeBrownEnergyConsumption,
                        double _cumulativeEnergyConsumption,
                        double _cumulativeGreenEnergyProduction,
                        double _cumulativeRemainingGreenEnergy){
        
        cumulativeBrownEnergyConsumption = _cumulativeBrownEnergyConsumption;
        cumulativeEnergyConsumption = _cumulativeEnergyConsumption;
        cumulativeGreenEnergyProduction = _cumulativeGreenEnergyProduction;
        cumulativeRemainingGreenEnergy = _cumulativeRemainingGreenEnergy;

    }

    double getCumulativeBrownEnergyConsumption()
    {
        return cumulativeBrownEnergyConsumption;

    }
    double getCumulativeEnergyConsumption()
    {
        return cumulativeEnergyConsumption;
    }
    double getCumulativeGreenEnergyProduction()
    {
        return cumulativeGreenEnergyProduction;
    }
    double getCumulativeRemainingGreenEnergy()
    {
        return cumulativeRemainingGreenEnergy;
    }        
};

#endif