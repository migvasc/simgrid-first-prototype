#ifndef VMREQUEST_HPP
#define VMREQUEST_HPP

#include <simgrid/s4u.hpp>

class VMRequest{

private:
    int ncores;
    int ramsize;
    double startTime = 0;
    std::string id;
    simgrid::s4u::VirtualMachine* vm;

public:
    /** 
     *
     * @tableofcontents
     *
     * A VMRequest represents a virtual machine (or a container) that hosts actors.
     * The total computing power that the contained actors can get is constrained to the virtual machine size.
     *
     */
    VMRequest(int cores, int ram, std::string vmID);
    int getRamSize();
    int getNCores();
    std::string getID();
    simgrid::s4u::VirtualMachine* getVM();
    void setVM(simgrid::s4u::VirtualMachine* vm_);


};

#endif