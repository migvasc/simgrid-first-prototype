#ifndef VMSHUTDOWNACTOR_HPP
#define VMSHUTDOWNACTOR_HPP
#include "simgrid/s4u.hpp"
#include "DeployableVM.hpp"

class DeployableVM;
class VMShutdownActor{
    private:
        DeployableVM *vm;        
    public:
        explicit VMShutdownActor() = default;  
        explicit VMShutdownActor(DeployableVM *avm);
        void operator()();
};

#endif